// import các thư viện
const express = require("express"); 
const mongoose = require("mongoose");
const path = require("path"); 
// khai báo các đối tượng 
const app = express();
// khai báo cổng
const port = 8000;
// import router
const userRouter = require("./app/routes/user.route");
const diceHistoryRouter = require("./app/routes/diceHistory.route");
const prizeRouter = require("./app/routes/prize.route");
const voucherRouter = require("./app/routes/voucher.route");
const prizeHistoryRouter = require("./app/routes/prizeHistory.route");
const voucherHistoryRouter = require("./app/routes/voucherHistory.route");

// kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDiceCasino")
.then(() => console.log("MongoDB connected successfully"))
.catch((err) => console.log(err.message));

// cấu hình để dùng json 
app.use(express.json());

// định nghĩa middleware để phục vụ file tĩnh
app.use(express.static("views"));

// Application Middleware
// Middleware console.log ra thời gian hiện tại
app.use((req, res, next) => {
  console.log("Thời gian hiện tại: ", new Date());
  next();
}) ;

// Middleware console.log ra request method   
app.use((req, res, next) => {
  console.log("Request method: ", req.method);
  next();
}) ;

// khi báo các api 
// api trả ra 1 số tự nhiên có giá trị từ 1 đến 6
app.get("/random-number", (req, res) => {
  var vRandomNumber = Math.floor((Math.random() * 6) + 1);
  console.log(vRandomNumber);
  return vRandomNumber;
});

// định nghĩa route cho trang chủ
app.get("/", (req, res) => {
  // đường dẫn đén file html trong thư mục views
  console.log(__dirname);
  const casinoPath = path.join(__dirname + "/views/luckyDiceCasinoNodejs.html");
  return res.sendFile(casinoPath);
});

// Router Middleware
app.use("/users", userRouter);
app.use("/dice-histories", diceHistoryRouter);
app.use("/prizes", prizeRouter);voucherRouter
app.use("/vouchers", voucherRouter);
app.use("/prize-histories", prizeHistoryRouter);
app.use("/voucher-histories", voucherHistoryRouter);

// listen
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});