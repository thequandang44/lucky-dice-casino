const getAllUserMiddleware = (req, res, next) => {
  console.log("Get all users middleware");
  next();
}

const createUserMiddleware = (req, res, next) => {
  console.log("Create user middleware");
  next();
}

const getUserByIdMiddleware = (req, res, next) => {
  console.log("Get user by id: " + req.params.userId + " middleware");
  next();
}

const updateUserByIdMiddleware = (req, res, next) => {
  console.log("Update user by id: " + req.params.userId + " middleware");
  next();
}

const deleteUserByIdMiddleware = (req, res, next) => {
  console.log("Delete user by id: " + req.params.userId + " middleware");
  next();
}

module.exports = {
  getAllUserMiddleware,
  createUserMiddleware,
  getUserByIdMiddleware,
  updateUserByIdMiddleware,
  deleteUserByIdMiddleware
}
