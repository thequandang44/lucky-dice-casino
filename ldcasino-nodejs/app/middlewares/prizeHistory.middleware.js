const getAllPrizeHistoryMiddleware = (req, res, next) => {
  console.log("Get all prize histories middleware");
  next();
}

const createPrizeHistoryMiddleware = (req, res, next) => {
  console.log("Create prize history middleware");
  next();
}

const getPrizeHistoryByIdMiddleware = (req, res, next) => {
  console.log("Get prize history by id: " + req.params.historyId + " middleware");
  next();
}

const updatePrizeHistoryByIdMiddleware = (req, res, next) => {
  console.log("Update prize history by id: " + req.params.historyId + " middleware");
  next();
}

const deletePrizeHistoryByIdMiddleware = (req, res, next) => {
  console.log("Delete prize history by id: " + req.params.historyId + " middleware");
  next();
}

module.exports = {
  getAllPrizeHistoryMiddleware,
  createPrizeHistoryMiddleware,
  getPrizeHistoryByIdMiddleware,
  updatePrizeHistoryByIdMiddleware,
  deletePrizeHistoryByIdMiddleware
}
