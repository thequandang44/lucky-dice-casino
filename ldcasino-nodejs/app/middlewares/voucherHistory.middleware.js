const getAllVoucherHistoryMiddleware = (req, res, next) => {
  console.log("Get all voucher histories middleware");
  next();
}

const createVoucherHistoryMiddleware = (req, res, next) => {
  console.log("Create voucher history middleware");
  next();
}

const getVoucherHistoryByIdMiddleware = (req, res, next) => {
  console.log("Get voucher history by id: " + req.params.historyId + " middleware");
  next();
}

const updateVoucherHistoryByIdMiddleware = (req, res, next) => {
  console.log("Update voucher history by id: " + req.params.historyId + " middleware");
  next();
}

const deleteVoucherHistoryByIdMiddleware = (req, res, next) => {
  console.log("Delete voucher history by id: " + req.params.historyId + " middleware");
  next();
}

module.exports = {
  getAllVoucherHistoryMiddleware,
  createVoucherHistoryMiddleware,
  getVoucherHistoryByIdMiddleware,
  updateVoucherHistoryByIdMiddleware,
  deleteVoucherHistoryByIdMiddleware
}
