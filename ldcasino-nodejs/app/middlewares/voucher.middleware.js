const getAllVoucherMiddleware = (req, res, next) => {
  console.log("Get all Voucher middleware");
  next();
}

const createVoucherMiddleware = (req, res, next) => {
  console.log("Create Voucher middleware");
  next();
}

const getVoucherByIdMiddleware = (req, res, next) => {
  console.log("Get Voucher by id: " + req.params.voucherId + " middleware");
  next();
}

const updateVoucherByIdMiddleware = (req, res, next) => {
  console.log("Update Voucher by id: " + req.params.voucherId + " middleware");
  next();
}

const deleteVoucherByIdMiddleware = (req, res, next) => {
  console.log("Delete Voucher by id: " + req.params.voucherId + " middleware");
  next();
}

module.exports = {
  getAllVoucherMiddleware,
  createVoucherMiddleware,
  getVoucherByIdMiddleware,
  updateVoucherByIdMiddleware,
  deleteVoucherByIdMiddleware
}
