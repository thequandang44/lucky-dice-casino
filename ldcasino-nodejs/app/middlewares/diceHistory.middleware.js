const getAllDiceHistoryMiddleware = (req, res, next) => {
  console.log("Get all dice histories middleware");
  next();
}

const createDiceHistoryMiddleware = (req, res, next) => {
  console.log("Create dice history middleware");
  next();
}

const getDiceHistoryByIdMiddleware = (req, res, next) => {
  console.log("Get dice history by id: " + req.params.diceHistoryId + " middleware");
  next();
}

const updateDiceHistoryByIdMiddleware = (req, res, next) => {
  console.log("Update dice history by id: " + req.params.diceHistoryId + " middleware");
  next();
}

const deleteDiceHistoryByIdMiddleware = (req, res, next) => {
  console.log("Delete dice history by id: " + req.params.diceHistoryId + " middleware");
  next();
}

module.exports = {
  getAllDiceHistoryMiddleware,
  createDiceHistoryMiddleware,
  getDiceHistoryByIdMiddleware,
  updateDiceHistoryByIdMiddleware,
  deleteDiceHistoryByIdMiddleware
}
