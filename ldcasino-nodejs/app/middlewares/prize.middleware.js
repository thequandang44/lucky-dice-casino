const getAllPrizeMiddleware = (req, res, next) => {
  console.log("Get all prize middleware");
  next();
}

const createPrizeMiddleware = (req, res, next) => {
  console.log("Create prize middleware");
  next();
}

const getPrizeByIdMiddleware = (req, res, next) => {
  console.log("Get prize by id: " + req.params.prizeId + " middleware");
  next();
}

const updatePrizeByIdMiddleware = (req, res, next) => {
  console.log("Update prize by id: " + req.params.prizeId + " middleware");
  next();
}

const deletePrizeByIdMiddleware = (req, res, next) => {
  console.log("Delete prize by id: " + req.params.prizeId + " middleware");
  next();
}

module.exports = {
  getAllPrizeMiddleware,
  createPrizeMiddleware,
  getPrizeByIdMiddleware,
  updatePrizeByIdMiddleware,
  deletePrizeByIdMiddleware
}
