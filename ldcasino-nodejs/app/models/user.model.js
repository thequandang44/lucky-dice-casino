const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // import schema từ mongoose

// khởi tạo user schema
const userSchema = new Schema({
  _id: mongoose.Types.ObjectId,
	username: {
    type: String,
    unique: true,
    required: true
  }, 
	firstname: {
    type: String,
    required: true
  }, 
	lastname: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default:  Date.now()
  },
  updatedAt: {
    type: Date,
    default:  Date.now()
  }
}, {
  timestamps: true
});

// export schema model
module.exports = mongoose.model("User", userSchema);