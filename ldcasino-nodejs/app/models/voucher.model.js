const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // import schema từ mongoose

// khởi tạo voucher schema
const voucherSchema = new Schema({
  _id: mongoose.Types.ObjectId,
	code: {
    type: String,
    required: true,
    unique: true
  }, 
  discount: {
    type: Number,
    required: true
  },
  note: {
    type: String,
    required: false
  },
  createdAt: {
    type: Date,
    default:  Date.now()
  },
  updatedAt: {
    type: String,
    default:  Date.now()
  }
}, {
  timestamps: true
});

// export schema model
module.exports = mongoose.model("Voucher", voucherSchema);