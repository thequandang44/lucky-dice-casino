const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // import schema từ mongoose

// khởi tạo prize schema
const prizeSchema = new Schema({
  _id: mongoose.Types.ObjectId,
	name: {
    type: String,
    required: true,
    unique: true
  }, 
  description: {
    type: String,
    required: false
  },
  createdAt: {
    type: Date,
    default:  Date.now()
  },
  updatedAt: {
    type: Date,
    default:  Date.now()
  }
}, {
  timestamps: true
});

// export schema model
module.exports = mongoose.model("Prize", prizeSchema);