const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // import schema từ mongoose

// khởi tạo voucher history schema
const voucherHistorySchema = new Schema({
	user: [
    {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true
    }
  ], 
	voucher: [
    {
      type: mongoose.Types.ObjectId,
      ref: "Voucher",
      required: true
    }
  ], 
  createdAt: {
    type: Date,
    default:  Date.now()
  },
  updatedAt: {
    type: Date,
    default:  Date.now()
  }
}, {
  timestamps: true
});

// export schema model
module.exports = mongoose.model("VoucherHistory", voucherHistorySchema);