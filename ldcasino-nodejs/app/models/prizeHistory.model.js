const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // import schema từ mongoose

// khởi tạo prize history schema
const prizeHistorySchema = new Schema({
	user: [
    {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true
    }
  ], 
	prize: [
    {
      type: mongoose.Types.ObjectId,
      ref: "Prize",
      required: true
    }
  ], 
  createdAt: {
    type: Date,
    default:  Date.now()
  },
  updatedAt: {
    type: Date,
    default:  Date.now()
  }
}, {
  timestamps: true
});

// export schema model
module.exports = mongoose.model("PrizeHistory", prizeHistorySchema);