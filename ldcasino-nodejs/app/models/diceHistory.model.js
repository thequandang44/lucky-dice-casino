const mongoose = require("mongoose"); // import mongoose
const Schema = mongoose.Schema; // import schema từ mongoose

// khởi tạo diceHistory schema
const diceHistorySchema = new Schema({
  _id: mongoose.Types.ObjectId,
	user: [
    {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true
    }
  ], 
	dice: {
    type: Number,
    required: true
  }, 
  createdAt: {
    type: Date,
    default:  Date.now()
  },
  updatedAt: {
    type: String,
    default:  Date.now()
  }
}, {
  timestamps: true
});

// export schema model
module.exports = mongoose.model("diceHistory", diceHistorySchema);