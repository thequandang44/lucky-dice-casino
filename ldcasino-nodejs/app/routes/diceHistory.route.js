const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// import middleware
const {
  getAllDiceHistoryMiddleware,
  createDiceHistoryMiddleware,
  getDiceHistoryByIdMiddleware,
  updateDiceHistoryByIdMiddleware,
  deleteDiceHistoryByIdMiddleware
} = require("../middlewares/diceHistory.middleware");

// import controller
const {
  getAllDiceHistory,
  createDiceHistory,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById
} = require("../controllers/diceHistory.controller");

// khai báo các api router
router.get("/", getAllDiceHistoryMiddleware, getAllDiceHistory);
router.post("/", createDiceHistoryMiddleware, createDiceHistory);
router.get("/:diceHistoryId", getDiceHistoryByIdMiddleware, getDiceHistoryById);
router.put("/:diceHistoryId", updateDiceHistoryByIdMiddleware, updateDiceHistoryById);
router.delete("/:diceHistoryId", deleteDiceHistoryByIdMiddleware, deleteDiceHistoryById);

// xuất router
module.exports = router;
