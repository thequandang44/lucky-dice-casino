const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// import middleware
const {
  getAllUserMiddleware,
  createUserMiddleware,
  getUserByIdMiddleware,
  updateUserByIdMiddleware,
  deleteUserByIdMiddleware
} = require("../middlewares/user.middleware");

// import controller
const {
  getAllUser,
  createUser,
  getUserById,
  updateUserById,
  deleteUserById
} = require("../controllers/user.controller");

// khai báo các api router
router.get("/", getAllUserMiddleware, getAllUser);
router.post("/", createUserMiddleware, createUser);
router.get("/:userId", getUserByIdMiddleware, getUserById);
router.put("/:userId", updateUserByIdMiddleware, updateUserById);
router.delete("/:userId", deleteUserByIdMiddleware, deleteUserById);

// xuất router
module.exports = router;
