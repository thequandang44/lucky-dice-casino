const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// import middleware
const {
  getAllPrizeMiddleware,
  createPrizeMiddleware,
  getPrizeByIdMiddleware,
  updatePrizeByIdMiddleware,
  deletePrizeByIdMiddleware
} = require("../middlewares/prize.middleware");

// import controller
const {
  getAllPrize,
  createPrize,
  getPrizeById,
  updatePrizeById,
  deletePrizeById
} = require("../controllers/prize.controller");

// khai báo các api router
router.get("/", getAllPrizeMiddleware, getAllPrize);
router.post("/", createPrizeMiddleware, createPrize);
router.get("/:prizeId", getPrizeByIdMiddleware, getPrizeById);
router.put("/:prizeId", updatePrizeByIdMiddleware, updatePrizeById);
router.delete("/:prizeId", deletePrizeByIdMiddleware, deletePrizeById);

// xuất router
module.exports = router;
