const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// import middleware
const {
  getAllPrizeHistoryMiddleware,
  createPrizeHistoryMiddleware,
  getPrizeHistoryByIdMiddleware,
  updatePrizeHistoryByIdMiddleware,
  deletePrizeHistoryByIdMiddleware
} = require("../middlewares/prizeHistory.middleware");

// import controller
const {
  getAllPrizeHistory,
  createPrizeHistory,
  getPrizeHistoryById,
  updatePrizeHistoryById,
  deletePrizeHistoryById
} = require("../controllers/prizeHistory.controller");

// khai báo các api router
router.get("/", getAllPrizeHistoryMiddleware, getAllPrizeHistory);
router.post("/", createPrizeHistoryMiddleware, createPrizeHistory);
router.get("/:historyId", getPrizeHistoryByIdMiddleware, getPrizeHistoryById);
router.put("/:historyId", updatePrizeHistoryByIdMiddleware, updatePrizeHistoryById);
router.delete("/:historyId", deletePrizeHistoryByIdMiddleware, deletePrizeHistoryById);

// xuất router
module.exports = router;
