const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// import middleware
const {
  getAllVoucherMiddleware,
  createVoucherMiddleware,
  getVoucherByIdMiddleware,
  updateVoucherByIdMiddleware,
  deleteVoucherByIdMiddleware
} = require("../middlewares/voucher.middleware");

// import controller
const {
  getAllVoucher,
  createVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById
} = require("../controllers/voucher.controller");

// khai báo các api router
router.get("/", getAllVoucherMiddleware, getAllVoucher);
router.post("/", createVoucherMiddleware, createVoucher);
router.get("/:voucherId", getVoucherByIdMiddleware, getVoucherById);
router.put("/:voucherId", updateVoucherByIdMiddleware, updateVoucherById);
router.delete("/:voucherId", deleteVoucherByIdMiddleware, deleteVoucherById);

// xuất router
module.exports = router;
