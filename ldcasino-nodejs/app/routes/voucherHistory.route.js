const express = require("express"); // import thư viện express
const router = express.Router(); // khởi tạo router

// import middleware
const {
  getAllVoucherHistoryMiddleware,
  createVoucherHistoryMiddleware,
  getVoucherHistoryByIdMiddleware,
  updateVoucherHistoryByIdMiddleware,
  deleteVoucherHistoryByIdMiddleware
} = require("../middlewares/voucherHistory.middleware");

// import controller
const {
  getAllVoucherHistory,
  createVoucherHistory,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById
} = require("../controllers/voucherHistory.controller");

// khai báo các api router
router.get("/", getAllVoucherHistoryMiddleware, getAllVoucherHistory);
router.post("/", createVoucherHistoryMiddleware, createVoucherHistory);
router.get("/:historyId", getVoucherHistoryByIdMiddleware, getVoucherHistoryById);
router.put("/:historyId", updateVoucherHistoryByIdMiddleware, updateVoucherHistoryById);
router.delete("/:historyId", deleteVoucherHistoryByIdMiddleware, deleteVoucherHistoryById);

// xuất router
module.exports = router;
