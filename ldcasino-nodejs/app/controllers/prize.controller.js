const mongoose = require("mongoose"); // import thư viện mongoose
const prizeModel = require("../models/prize.model"); // import model Prize

const getAllPrize = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dũ liệu
  try {
    const result = await prizeModel.find();

    return res.status(200).json({
      message: "Lấy danh sách prize thành công",
      data: result 
    });
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const createPrize = async (req, res) => {
  // b1: thu thập dữ liệu
  const{
    name,
    description
  } = req.body;
  // b2: validate dữ liệu
  if (!name) {
    return res.json({
      message: "Xin nhập tên phần thưởng"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const newPrize = {
      _id: new mongoose.Types.ObjectId(),
      name,
      description
    };

    const result = await prizeModel.create(newPrize);

    return res.status(201).json({
      message: "Tạo prize thành công",
      data: result
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getPrizeById = async (req, res) => {
  // b1: thu thập dữ liệu
  const prizeId = req.params.prizeId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    return res.status(400).json({
      message: "prize ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await prizeModel.findById(prizeId);

    if (result) {
      return res.status(200).json({
        message: "Lấy prize theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy prize theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const updatePrizeById = async (req, res) => {
  // b1: thu thập dữ liệu
  const prizeId = req.params.prizeId;
  const {
    name,
    description
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    return res.status(400).json({
      message: "prize ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const updatedPrize = {
      name,
      description
    };

    const result = await prizeModel.findByIdAndUpdate(prizeId, updatedPrize);

    if (result) {
      return res.status(200).json({
        message: "Cập nhật prize theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy prize theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const deletePrizeById = async (req, res) => {
  // b1: thu thập dữ liệu
  const prizeId = req.params.prizeId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    return res.status(400).json({
      message: "prize ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await prizeModel.findByIdAndDelete(prizeId);

    if (result) {
      return res.status(200).json({
        message: "Xóa prize theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy prize theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

module.exports = {
  getAllPrize,
  createPrize,
  getPrizeById,
  updatePrizeById,
  deletePrizeById
};