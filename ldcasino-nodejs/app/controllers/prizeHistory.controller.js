const mongoose = require("mongoose"); // import thư viện mongoose
const prizeHistoryModel = require("../models/prizeHistory.model"); // import model prize history

const getAllPrizeHistory = async (req, res) => {
  // b1: thu thập dữ liệu 
  const user = req.query.user;

  // b2: validate dữ liệu 
  if (user && !mongoose.Types.ObjectId.isValid(user)) {
    return res.json({
      message: "User không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const prizeHistoryList = await prizeHistoryModel.find();
    // nếu ko có user query 
    if (!user) {
      return res.status(200).json({
        message: "Lấy danh sách prize history thành công",
        data: prizeHistoryList 
      });
    }

    // nếu có user query
    const userPrizeList = [];
    prizeHistoryList.forEach(prize => { // chạy qua danh sách prizeHistory
    const userObjList = prize.user;  // lấy ra user của từng phân tử trong danh sách
    const userObjIdArr = userObjList.filter(userObjId => userObjId == user); // tìm user giống user query
    console.log(userObjIdArr.length)
    if (userObjIdArr.length > 0) {  
      userPrizeList.push(prize);
    }
    })
    if (userPrizeList.length > 0) {
      return res.json({
        message: "Lấy danh sách prize history của user thành công",
        data: userPrizeList 
      })
    } else {
      return res.json({
        message: "Không tìm thấy prize history nào của user",
        data: userPrizeList 
      })
    }
    
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const createPrizeHistory = async (req, res) => {
  // b1: thu thập dữ liệu
  const {
    userId,
    prizeId
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    });
  }
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    return res.status(400).json({
      message: "Prize ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const newPrizeHistory = {
      user: userId,
      prize: prizeId
    };

    const result = await prizeHistoryModel.create(newPrizeHistory);

    return res.status(201).json({
      message: "Tạo prize history thành công",
      data: result
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getPrizeHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const historyId = req.params.historyId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    return res.status(400).json({
      message: "Prize history ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await prizeHistoryModel.findById(historyId);

    if (result) {
      return res.status(200).json({
        message: "Lấy prize history theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy prize history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const updatePrizeHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const historyId = req.params.historyId;
  const {
    userId,
    prizeId
  } = req.body;

  // b2: validate dữ liệu
  if (userId && !mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    });
  }
  if (prizeId && !mongoose.Types.ObjectId.isValid(prizeId)) {
    return res.status(400).json({
      message: "Prize ID không hợp lệ"
    });
  }
  
  // b3: xử lí dữ liệu
  try {
    const updatedPrizeHistory = {
      user: userId,
      prize: prizeId
    };

    const result = await prizeHistoryModel.findByIdAndUpdate(historyId, updatedPrizeHistory);

    if (result) {
      return res.status(200).json({
        message: "Cập nhật prize history theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy prize history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const deletePrizeHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const historyId = req.params.historyId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    return res.status(400).json({
      message: "Prize history ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await prizeHistoryModel.findByIdAndDelete(historyId);

    if (result) {
      return res.status(200).json({
        message: "Xóa prize history theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy prize history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

module.exports = {
  getAllPrizeHistory,
  createPrizeHistory,
  getPrizeHistoryById,
  updatePrizeHistoryById,
  deletePrizeHistoryById
};