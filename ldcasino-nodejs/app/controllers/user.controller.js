const mongoose = require("mongoose"); // import thư viện mongoose
const diceHistoryModel = require("../models/diceHistory.model"); // import model dice history
const userModel = require("../models/user.model"); // import model user

const getAllUser = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const diceHistoryId = req.query.diceHistoryId;

  // b2: validate dữ liệu (ko có)
  if (diceHistoryId !== undefined && !mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return res.status(400).json({
      message: "Dice history ID không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const populatedDiceHistory = await diceHistoryModel.findById(diceHistoryId).populate("user");
    if (diceHistoryId === undefined) {
      const userList = await userModel.find();
      if (userList && userList.length > 0) {
        return res.status(200).json({
          message: "Lấy danh sách user thành công",
          data: userList
        })
      } else {
        return res.status(404).json({
          message: "Không tìm thấy user nào"
        })
      }
    } 
    else if (populatedDiceHistory) {
      return res.status(200).json({
        message: "Lấy tất cả user của dice history thành công",
        data: populatedDiceHistory.user
      })
    } 
    else {
      return res.status(404).json({
        message: "Không tìm thấy dice history phù hợp",
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const createUser = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const {
    diceHistoryId,
    username,
    firstname,
    lastname
  } = req.body;

  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return res.status(400).json({
      message: "Dice history ID không hợp lệ"
    })
  }
  if (!username) {
    return res.status(400).json({
      message: "Xin mời nhập tên người dùng"
    });
  }
  if (!firstname) {
    return res.status(400).json({
      message: "Xin mời nhập tên riêng"
    });
  }
  if (!lastname) {
    return res.status(400).json({
      message: "Xin mời nhập họ"
    });
  }

  // b3: xử lí dũ liệu
  const newUser = {
    _id: new mongoose.Types.ObjectId(),
    username,
    firstname,
    lastname
  }
  try {
    const createdUser = await userModel.create(newUser);
    const updatedDiceHistory = await diceHistoryModel.findByIdAndUpdate(diceHistoryId, {
      $push: { user: createdUser._id }
    });
    return res.status(201).json({
      message:"Tạo user và đẩy vào dice history thành công",
      data: createdUser,
      diceHistory: updatedDiceHistory
    })
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const getUserById = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const userId = req.params.userId;

  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const foundUser = await userModel.findById(userId);
    if (foundUser) {
      return res.status(200).json({
        message: "Lấy user theo id thành công",
        data: foundUser
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy user tương ứng id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const updateUserById = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const userId = req.params.userId;
  const {
    username,
    firstname,
    lastname
  } = req.body;
  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const updatedUser = {
      username,
      firstname,
      lastname
    };
    const result = await userModel.findByIdAndUpdate(userId, updatedUser);
    if (result) {
      return res.status(200).json({
        message: "Cập nhật user theo id thành công",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy user tương ứng id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

const deleteUserById = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  const userId = req.params.userId;
  const diceHistoryId = req.query.diceHistoryId;

  // b2: validate dữ liệu (ko có)
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    if (diceHistoryId !== undefined) {
      await diceHistoryModel.findByIdAndUpdate(diceHistoryId, {
        $pull: { user: userId }
      })
    }

    const deletedUser = await userModel.findByIdAndDelete(userId);
    if (deletedUser) {
      return res.status(200).json({
        message: "Xóa user theo id thành công"
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy user tương ứng id"
      })
    }
  } catch (error) {
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
};

module.exports = {
  getAllUser,
  createUser,
  getUserById,
  updateUserById,
  deleteUserById
};