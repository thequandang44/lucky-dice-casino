const mongoose = require("mongoose"); // import thư viện mongoose
const diceHistoryModel = require("../models/diceHistory.model"); // import model diceHistory

const getAllDiceHistory = async (req, res) => {
  // b1: thu thập dữ liệu 
  const user = req.query.user;

  // b2: validate dữ liệu 
  if (user && !mongoose.Types.ObjectId.isValid(user)) {
    return res.json({
      message: "User không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const diceHistoryList = await diceHistoryModel.find();
    // nếu ko có user query 
    if (!user) {
      return res.status(200).json({
        message: "Lấy danh sách dice history thành công",
        data: diceHistoryList 
      });
    }

    // nếu có user query
    const userDiceList = [];
    diceHistoryList.forEach(dice => { // chạy qua danh sách diceHistory
    const userObjList = dice.user;  // lấy ra user của từng phân tử trong danh sách
    const userObjIdArr = userObjList.filter(userObjId => userObjId == user); // tìm user giống user query
    console.log(userObjIdArr.length)
    if (userObjIdArr.length > 0) {  
      userDiceList.push(dice);
    }
    })
    if (userDiceList.length > 0) {
      return res.json({
        message: "Lấy danh sách dice history của user thành công",
        data: userDiceList 
      })
    } else {
      return res.json({
        message: "Không tìm thấy dice history nào của user",
        data: userDiceList 
      })
    }
    
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const createDiceHistory = async (req, res) => {
  // b1: thu thập dữ liệu
  const dice = Math.floor((Math.random() * 6) + 1);
  const {
    user
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(user)) {
    return res.json({
      message: "User không hợp lệ"
    })
  }

  // b3: xử lí dữ liệu
  try {
    const newDiceHistory = {
      _id: new mongoose.Types.ObjectId(),
      dice,
      user
    };

    const result = await diceHistoryModel.create(newDiceHistory);

    return res.status(201).json({
      message: "Tạo dice history thành công",
      data: result
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getDiceHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const diceHistoryId = req.params.diceHistoryId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return res.status(400).json({
      message: "Dice history ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await diceHistoryModel.findById(diceHistoryId);

    if (result) {
      return res.status(200).json({
        message: "Lấy dice history theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy dice history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const updateDiceHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const diceHistoryId = req.params.diceHistoryId;
  const {
    dice,
    user
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return res.status(400).json({
      message: "Dice history ID không hợp lệ"
    });
  }
  if (parseInt(dice) < 1 || parseInt(dice) > 6) {
    return res.status(400).json({
      message: "Dice phải trong khoảng từ 1 đến 6"
    })
  }
  if (!mongoose.Types.ObjectId.isValid(user)) {
    return res.json({
      message: "User không hợp lệ"
    })
  }

  // b3: xử lí dữ liệu
  try {
    const updatedDiceHistory = {
      dice,
      user
    };

    const result = await diceHistoryModel.findByIdAndUpdate(diceHistoryId, updatedDiceHistory);

    if (result) {
      return res.status(200).json({
        message: "Cập nhật dice history theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy dice history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const deleteDiceHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const diceHistoryId = req.params.diceHistoryId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return res.status(400).json({
      message: "Dice history ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await diceHistoryModel.findByIdAndDelete(diceHistoryId);

    if (result) {
      return res.status(200).json({
        message: "Xóa dice history theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy dice history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

module.exports = {
  getAllDiceHistory,
  createDiceHistory,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById
};