const mongoose = require("mongoose"); // import thư viện mongoose
const voucherModel = require("../models/Voucher.model"); // import model Voucher

const getAllVoucher = async (req, res) => {
  // b1: thu thập dữ liệu (ko có)
  // b2: validate dữ liệu (ko có)
  // b3: xử lí dũ liệu
  try {
    const result = await voucherModel.find();

    return res.status(200).json({
      message: "Lấy danh sách Voucher thành công",
      data: result 
    });
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const createVoucher = async (req, res) => {
  // b1: thu thập dữ liệu
  const {
    code,
    discount,
    note
  } = req.body;

  // b2: validate dữ liệu
  if (!code) {
    return res.json({
      message: "Xin nhập tên mã giảm giá"
    });
  }
  if (!discount) {
    return res.json({
      message: "Xin nhập phần trăm giảm giá"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const newVoucher = {
      _id: new mongoose.Types.ObjectId(),
      code,
      discount,
      note
    };

    const result = await voucherModel.create(newVoucher);

    return res.status(201).json({
      message: "Tạo Voucher thành công",
      data: result
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getVoucherById = async (req, res) => {
  // b1: thu thập dữ liệu
  const voucherId = req.params.voucherId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return res.status(400).json({
      message: "Voucher ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await voucherModel.findById(voucherId);

    if (result) {
      return res.status(200).json({
        message: "Lấy Voucher theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy Voucher theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const updateVoucherById = async (req, res) => {
  // b1: thu thập dữ liệu
  const voucherId = req.params.voucherId;
  const {
    code,
    discount,
    note
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return res.status(400).json({
      message: "Voucher ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const updatedVoucher = {
      code,
      discount,
      note
    };

    const result = await voucherModel.findByIdAndUpdate(voucherId, updatedVoucher);

    if (result) {
      return res.status(200).json({
        message: "Cập nhật Voucher theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy Voucher theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const deleteVoucherById = async (req, res) => {
  // b1: thu thập dữ liệu
  const voucherId = req.params.voucherId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return res.status(400).json({
      message: "Voucher ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await voucherModel.findByIdAndDelete(voucherId);

    if (result) {
      return res.status(200).json({
        message: "Xóa Voucher theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy Voucher theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

module.exports = {
  getAllVoucher,
  createVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById
};