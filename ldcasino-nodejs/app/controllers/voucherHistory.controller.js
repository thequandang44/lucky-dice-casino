const mongoose = require("mongoose"); // import thư viện mongoose
const voucherHistoryModel = require("../models/voucherHistory.model"); // import model voucher history

const getAllVoucherHistory = async (req, res) => {
  // b1: thu thập dữ liệu 
  const user = req.query.user;

  // b2: validate dữ liệu 
  if (user && !mongoose.Types.ObjectId.isValid(user)) {
    return res.json({
      message: "User không hợp lệ"
    })
  }

  // b3: xử lí dũ liệu
  try {
    const voucherHistoryList = await voucherHistoryModel.find();
    // nếu ko có user query 
    if (!user) {
      return res.status(200).json({
        message: "Lấy danh sách voucher history thành công",
        data: voucherHistoryList 
      });
    }

    // nếu có user query
    const userVoucherList = [];
    voucherHistoryList.forEach(voucher => { // chạy qua danh sách voucherHistory
    const userObjList = voucher.user;  // lấy ra user của từng phân tử trong danh sách
    const userObjIdArr = userObjList.filter(userObjId => userObjId == user); // tìm user giống user query
    console.log(userObjIdArr.length)
    if (userObjIdArr.length > 0) {  
      userVoucherList.push(voucher);
    }
    })
    if (userVoucherList.length > 0) {
      return res.json({
        message: "Lấy danh sách voucher history của user thành công",
        data: userVoucherList 
      })
    } else {
      return res.json({
        message: "Không tìm thấy voucher history nào của user",
        data: userVoucherList 
      })
    }
    
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const createVoucherHistory = async (req, res) => {
  // b1: thu thập dữ liệu
  const {
    userId,
    voucherId
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    });
  }
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return res.status(400).json({
      message: "Voucher ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const newVoucherHistory = {
      user: userId,
      voucher: voucherId
    };

    const result = await voucherHistoryModel.create(newVoucherHistory);

    return res.status(201).json({
      message: "Tạo voucher history thành công",
      data: result
    });
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const getVoucherHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const historyId = req.params.historyId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    return res.status(400).json({
      message: "Voucher history ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await voucherHistoryModel.findById(historyId);

    if (result) {
      return res.status(200).json({
        message: "Lấy voucher history theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy voucher history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const updateVoucherHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const historyId = req.params.historyId;
  const {
    userId,
    voucherId
  } = req.body;

  // b2: validate dữ liệu
  if (userId && !mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID không hợp lệ"
    });
  }
  if (voucherId && !mongoose.Types.ObjectId.isValid(voucherId)) {
    return res.status(400).json({
      message: "Voucher ID không hợp lệ"
    });
  }
  
  // b3: xử lí dữ liệu
  try {
    const updatedVoucherHistory = {
      user: userId,
      voucher: voucherId
    };

    const result = await voucherHistoryModel.findByIdAndUpdate(historyId, updatedVoucherHistory);

    if (result) {
      return res.status(200).json({
        message: "Cập nhật voucher history theo id thành công",
        data: result
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy voucher history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

const deleteVoucherHistoryById = async (req, res) => {
  // b1: thu thập dữ liệu
  const historyId = req.params.historyId;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(historyId)) {
    return res.status(400).json({
      message: "Voucher history ID không hợp lệ"
    });
  }

  // b3: xử lí dữ liệu
  try {
    const result = await voucherHistoryModel.findByIdAndDelete(historyId);

    if (result) {
      return res.status(200).json({
        message: "Xóa voucher history theo id thành công"
      });
    } else {
      return res.status(404).json({
        message: "Không tìm thấy voucher history theo id tương ứng"
      });
    }
  } catch (error) {
    // Dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    });
  }
};

module.exports = {
  getAllVoucherHistory,
  createVoucherHistory,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById
};